package com.example.primera_aplicacion

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    lateinit var startButton : Button
    lateinit var playerName : EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startButton = findViewById(R.id.start_button)
        playerName = findViewById(R.id.player_name_input)

        startButton.setOnClickListener { showGameActivity() }


    }

    fun showGameActivity(){

        val gameActivityIntent = Intent(this, GameActivity::class.java)
        gameActivityIntent.putExtra("PLAYER_NAME", playerName.text.toString())  //la llave seria "PLAYER_NAME" y el valor lo que esta al lado  playerName.text.toString()
        startActivity(gameActivityIntent)

    }

    companion object{ // se crea una constante  INTENT_PLAYER_NAME y no se necesita el nombre de la clase para crearla ya que es un companion object
        const val INTENT_PLAYER_NAME = "playerName"
    }

}
