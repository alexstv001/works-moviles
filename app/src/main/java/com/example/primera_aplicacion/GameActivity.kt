package com.example.primera_aplicacion

import android.icu.text.CaseMap
import android.media.AsyncPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class GameActivity : AppCompatActivity() {

    private lateinit var activityTitle: TextView
    private lateinit var playerName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        activityTitle = findViewById(R.id.game_title)
        //playerName = intent.getStringExtra("PLAYER_NAME")?: "Error getting player Name"
        playerName = intent.getStringExtra(MainActivity.INTENT_PLAYER_NAME) ?: "ERROR" //cuando INTENT_PLAYER_NAME es un opcional se pueden realizar muchas cosas para llamarle --> ?:"ERROR"
        //activityTitle.text = "Get Ready $playerName"  //como esto da un warninng, se puede crear un string y llamarlo de la siguiente forma
        activityTitle.text = getString(R.string.get_ready_player, playerName)

    }
}
